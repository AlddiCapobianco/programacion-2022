let template = "";
let miInfo, datos;

let borrar_localStorage = function(){
    localStorage.clear(); // PARA BORRAR TODO EL CONTENIDO DE LOCALSTORAGE
localStorage.removeItem("miInfo");
}

let leer_localStorage = function(){
    if (localStorage.getItem("miInfo")){
        miInfo = localStorage.getItem("miInfo");
        // console.log(miInfo);
        datos = JSON.parse(miInfo);
        // console.log(datos);

        template = `<h1>BIENVENIDO ${datos.nombre} ${datos.apellido}</h1>`;
    }else{
        template = `<h1>No se encontró el contenido en Local Storage</h1>`;
    }
    document.querySelector("#contenedorHeader").innerHTML = template;

}

leer_localStorage();