/* El objeto Storage(API(Aplication Programming Interface) de almacenamiento web) nos permite almacenar datos de manera local en el navegador, sin necesidada de realizar conexión alguna a base de datos.

LocalStorage y SessionStorage son propiedades que acceden al objeto Storage y tienen la funcion de almacenar datos de manera local, la diferencia entre estas dos es que LocalStorage almacena la informacion de manera indefinida, o hasta que se decida limpiar los datos del  navegador, y SessionStorage almacena informacion mientras la pestaña donde se esta utilizando sigo abierta, una vez cerrada, se elimina.

La  caracteristicas de LocalStorage y SessionStorage son:
- Permite almacemar entre 5 y 10 Mb de informacion, incluyendo texto y multimedia
- La informacion esta alamecnada en la computadora del cliente y NO es enviada en cada peticion al servidor, a diferencia de las cookies.
- Utilizan un minimo de peticiones al servidor para reducir el trafico de la red.
- Previenen perdidas de informacion ante una desconexion de la red
- La informacion es guardada por dominio web (incluye todas las paginas del dominio).
- LocalStorage solo soporta strings. */

//VERIFICAR SI STORAGE ES COMPATIBLE CON NUESTRO NAVEGADOR
if (typeof(Storage) !== "undefined"){
    console.log(Storage);
}else{
    console.log("Storage no es compatible con este navegador.");
}

let dataRead = function(){
    //ARMADO DE OBJETO CON LOS DATOS DE LA INTERFAZ
    let datos = {
        nombre: document.querySelector("#nombre").value,
        apellido: document.querySelector("#apellido").value,
        email: document.querySelector("#email").value,
        password: document.querySelector("#pass").value,
    }

    //GUARDAR EL OBJETO DATOS EN LOCALSTORAGE (PERSISTENCIA DE DATOS)
    grabar_localStorage(datos);
}

let grabar_localStorage = function(datos){
localStorage.setItem("miInfo", JSON.stringify(datos));
}

let captura_boton = function () {
    document.querySelector(".botones input").setAttribute("onclick", "dataRead()");
}

let main = function () {
    captura_boton();
}
main();